﻿using UnityEngine;

public class Figures : MonoBehaviour, IPooledObject
{
    public ObjectPooler.ObjectInfo.ObjectType Type => type;

    [SerializeField]
    private ObjectPooler.ObjectInfo.ObjectType type;

    private float lifeTime = 3.5f;
    private float speed = 3.5f;

    private float currentLifeTime;


    public void OnCreate(Vector3 position, Quaternion rotation)
    {
        transform.position = position;
        transform.rotation = rotation;
        currentLifeTime = lifeTime;
    }

    void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * speed);

        if ((currentLifeTime -= Time.deltaTime) < 0)
            ObjectPooler.Instance.DestroyObject(gameObject);
    }
}
