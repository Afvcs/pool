﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Shooting : MonoBehaviour
{
    [SerializeField]
    private Vector3 spawnPosition;
    [SerializeField]
    private ObjectPooler.ObjectInfo.ObjectType bulletType;
    [SerializeField]
    private Image btnCube, btnCapsule, btnCylinder, btnSphere;
    [SerializeField]
    private Text textCube, textCapsule, textCylinder, textSphere;
    private GameObject bullet;
    private Color redColor = Color.red, greenColor = Color.green;
    private int countCube, countCapsule, countCylinder, countSphere = 0;

    void Start()
    {
        BtnColor();
        StartCoroutine(Shoot());
        bulletType = ObjectPooler.ObjectInfo.ObjectType.Cube;
        btnCube.color = greenColor;
    }
    public void clickBtn(string type)
    {
        switch (type)
        {
            case "Cube":
                bulletType = ObjectPooler.ObjectInfo.ObjectType.Cube;
                BtnColor();
                btnCube.color = greenColor;
                break;
            case "Capsule":
                bulletType = ObjectPooler.ObjectInfo.ObjectType.Capsule;
                BtnColor();
                btnCapsule.color = greenColor;
                break;
            case "Cylinder":
                bulletType = ObjectPooler.ObjectInfo.ObjectType.Cylinder;
                BtnColor();
                btnCylinder.color = greenColor;
                break;
            case "Sphere":
                bulletType = ObjectPooler.ObjectInfo.ObjectType.Sphere;
                BtnColor();
                btnSphere.color = greenColor;
                break;
            default:
                Debug.Log("Такой фигуры нет");
                break;
        }
    }
    private void BtnColor()
    {
        btnCube.color = redColor;
        btnCapsule.color = redColor;
        btnCylinder.color = redColor;
        btnSphere.color = redColor;
    }

    IEnumerator Shoot()
    {
        while (true)
        {
            switch (bulletType)
            {
                case ObjectPooler.ObjectInfo.ObjectType.Cube:
                    countCube++;
                    textCube.text = ("X " + countCube.ToString("N0"));
                    break;
                case ObjectPooler.ObjectInfo.ObjectType.Capsule:
                    countCapsule++;
                    textCapsule.text = ("X " + countCapsule.ToString("N0"));
                    break;
                case ObjectPooler.ObjectInfo.ObjectType.Cylinder:
                    countCylinder++;
                    textCylinder.text = ("X " + countCylinder.ToString("N0"));
                    break;
                case ObjectPooler.ObjectInfo.ObjectType.Sphere:
                    countSphere++;
                    textSphere.text = ("X " + countSphere.ToString("N0"));
                    break;

                default:
                    Debug.Log("Такой фигуры нет");
                    break;
            }
            bullet = ObjectPooler.Instance.GetObject(bulletType);
            bullet.GetComponent<Figures>().OnCreate(spawnPosition, transform.rotation);
            yield return new WaitForSeconds(1.75f);
        }
    }

}
